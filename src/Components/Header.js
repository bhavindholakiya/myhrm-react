import React from "react";
import { Nav, Navbar, NavDropdown } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
    faBuilding, faCertificate,
    faClipboardList,
    faCog, faFileAlt, faKey, faPowerOff,
    faTachometerAlt,
    faUserCog,
    faUserEdit,
    faUsers
} from "@fortawesome/free-solid-svg-icons";
import Image from "react-bootstrap/Image";

function Header(){

    let currentLocation = window.location.pathname;
    const settingIcon = (<div style={{display: "inline-block"}}><FontAwesomeIcon icon={faCog} /> Settings </div>);
    const userSettingIcon = (<div style={{display: "inline-block"}}><FontAwesomeIcon icon={faUserCog} /> Admin Settings </div>);


    return(
        <Navbar collapseOnSelect expand="lg" bg="light" sticky="top" className="shadow-sm">
            <Navbar.Brand className="font-weight-bold" href="/">My HRM</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link active={currentLocation === '/dashboard' ? true : false} href="/dashboard">
                        <FontAwesomeIcon icon={faTachometerAlt}/> Dashboard</Nav.Link>
                    <Nav.Link active={currentLocation === '/employee' ? true : false} href="/employee">
                        <FontAwesomeIcon icon={faUsers}/> Employees</Nav.Link>
                    <Nav.Link active={currentLocation === '/my-leaves' ? true : false} href="/my-leaves">
                        <FontAwesomeIcon icon={faClipboardList}/> My Leaves</Nav.Link>

                    <NavDropdown active={window.location.pathname.includes('settings') ? true : false}
                                 title={settingIcon} id="collasible-nav-dropdown">
                        <NavDropdown.Item active={currentLocation === '/settings/departments' ? true : false} href="/settings/departments">
                            <FontAwesomeIcon icon={faBuilding}/> Departments</NavDropdown.Item>
                        <NavDropdown.Item active={currentLocation === '/settings/designations' ? true : false} href="/settings/designations">
                            <FontAwesomeIcon icon={faCertificate}/> Designation</NavDropdown.Item>
                        <NavDropdown.Item active={currentLocation === '/settings/leave-types' ? true : false} href="/settings/leave-types">
                            <FontAwesomeIcon icon={faFileAlt}/> Leave Types</NavDropdown.Item>
                    </NavDropdown>
                    <NavDropdown title={userSettingIcon} id="collasible-nav-dropdown2">
                        <NavDropdown.Item href="#action/3.1">Company Settings</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Email Templates</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">Audit Logs</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
                <Nav>
                    <Image src="src/assets/Images/images1.jpg" rounded />
                    <NavDropdown alignRight active title="Bhavin Dholakiya" id="collasible-nav-dropdown2">
                        <NavDropdown.Item href="#action/3.1">
                            <FontAwesomeIcon icon={faUserEdit}/> My Profile</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">
                            <FontAwesomeIcon icon={faKey}/> Change Password</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">
                            <FontAwesomeIcon icon={faPowerOff}/> Logout</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default Header;