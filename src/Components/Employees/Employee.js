import React, { Component } from 'react';
import { Button, Table } from "react-bootstrap";

class Employee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employee: []
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="content-wrap mt-3">
                    <div className="row mb-3">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <h4 className="font-weight-bold">Employees</h4>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <Button variant="dark" className="float-right">+ Add Employee</Button>
                        </div>
                    </div>
                    <Table bordered hover responsive size="sm">
                        <thead className="thead-dark">
                            <tr>
                                <th>Emp ID</th>
                                <th>Employee Name</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>Status</th>
                                <th>Joined At</th>
                                <th>Updated By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>EMP0001</td>
                                <td>Prayag Dholakiya</td>
                                <td>Managing Director</td>
                                <td>Management</td>
                                <td>Current</td>
                                <td>22/11/2019</td>
                                <td>25/04/2020</td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            </div>
        );
    }
}

export default Employee;