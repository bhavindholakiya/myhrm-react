import React, { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileAlt } from "@fortawesome/free-solid-svg-icons";
import { Button, Table } from "react-bootstrap";
import AddLeaveType from "./AddLeaveType";
import SuccessToast from '../../Utilities/SuccessToast';

function LeaveTypes() {

    const [showModal, setShowModal] = useState(false);
    const [showToast, setShowToast] = useState(false);

    const handleShow = () => setShowModal(true);
    const handleClose = () => setShowModal(false);
    const handleShowToast = () => setShowToast(true);
    const handleCloseToast = () => setShowToast(false);

    return (
        <div className="container-fluid">
            <div className="content-wrap mt-3">
                <div className="row mb-3">
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <h4 className="font-weight-bold">
                            <FontAwesomeIcon icon={faFileAlt} /> Leave Types</h4>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">

                        <Button variant="dark" className="float-right mx-1" onClick={handleShowToast}>Show Toast</Button>
                        <SuccessToast
                            show={showToast}
                            onHide={handleCloseToast}
                        />

                        <Button variant="dark" className="float-right" onClick={handleShow}>+ Add Leave type</Button>
                        <AddLeaveType
                            show={showModal}
                            onHide={handleClose}
                        />
                    </div>
                </div>
                <Table bordered hover responsive size="sm">
                    <thead className="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Leave type name</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Updated By</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Paid Leave</td>
                            <td>Active</td>
                            <td>22/11/2019</td>
                            <td>25/04/2020</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Sick Leave</td>
                            <td>Active</td>
                            <td>22/11/2019</td>
                            <td>25/04/2020</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </div>
    );

}

export default LeaveTypes;