import React, {useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSave, faTimes} from "@fortawesome/free-solid-svg-icons";

function AddLeaveType(props) {

    const [show, setShow] = useState(props.showModal);

    const handleClose = () => setShow(props.onHide);

    return (
        <Modal show={props.show} centered backdrop={"static"} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Create Leave Type</Modal.Title>
            </Modal.Header>
            <Form>
                <Modal.Body>
                    <Form.Group controlId="leaveTypeName">
                        <Form.Label>Leave Type Name</Form.Label>
                        <Form.Control
                            type="text"
                            name="leaveTypeName"
                            required
                            placeholder="Enter leave type name"/>
                    </Form.Group>
                    <Form.Group controlId="leaveTypeStatus">
                        <Form.Label>Active</Form.Label>
                        <Form.Check
                            name="leaveTypeStatus"
                            type="switch"
                            id="custom-switch"
                            label=" "
                        />
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success">
                        <FontAwesomeIcon icon={faSave}/> Save
                    </Button>
                    <Button variant="secondary" onClick={handleClose}>
                        <FontAwesomeIcon icon={faTimes}/> Close
                    </Button>
                </Modal.Footer>
            </Form>
        </Modal>
    )
}

export default AddLeaveType;