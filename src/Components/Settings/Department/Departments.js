import React, {Component} from 'react';
import {Button, Spinner, Table} from "react-bootstrap";
import AddDepartment from "./AddDepartment";
import {
    faBuilding, faEdit,
    faPlus, faTrash
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class Departments extends Component {

    constructor(props) {
        super(props);
        this.state = {
            deps: [],
            show: false
        }
    }

    componentDidMount() {
        this.getDepartments();
    }

    getDepartments() {
        fetch('http://localhost:3000/departments')
            .then(response => response.json())
                .then(result => {
                    this.setState({deps: result});
                });
    }

    DeleteDepartment(id){

    }

    HandleModal(){
        this.setState({show: !this.state.show})
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="content-wrap mt-3">
                    <div className="row  mb-3">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <h5 className="font-weight-bold">
                                <FontAwesomeIcon icon={faBuilding}/> Departments</h5>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <Button variant="dark" className="float-right" onClick={()=> this.HandleModal()} >
                                <FontAwesomeIcon icon={faPlus}/> Add Department</Button>
                            <AddDepartment
                                showModal={this.state.show}
                                hideModal={() => this.HandleModal()}
                            />
                        </div>
                    </div>
                    {this.state.deps ?
                        <Table bordered hover responsive size="sm">
                            <thead className="thead-dark">
                            <tr>
                                <th>Department ID</th>
                                <th>Department Name</th>
                                <th>Parent Department</th>
                                <th>Status</th>
                                <th>Updated At</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.deps.map(dept =>
                                    <tr key={dept.id}>
                                        <td>{dept.id}</td>
                                        <td>{dept.DeptName}</td>
                                        <td>{dept.Parent}</td>
                                        <td>{dept.Status ===  true ? 'Active' : 'Inactive'}</td>
                                        <td>{dept.Updated_At}</td>
                                        <td>{dept.Created_At}</td>
                                        <td>
                                            <Button variant={"outline-success"} className="mr-1 btn-sm" href={'edit'}>
                                                <FontAwesomeIcon icon={faEdit}/></Button>
                                            <Button variant={"outline-danger"} className="btn-sm" onClick={()=>this.DeleteDepartment(dept.DeptId)}>
                                                <FontAwesomeIcon icon={faTrash}/>
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </Table> :
                        <Spinner animation="border" role="status">
                            <span className="sr-only">Loading...</span>
                        </Spinner>}
                </div>
            </div>
        );
    }
}

export default Departments;