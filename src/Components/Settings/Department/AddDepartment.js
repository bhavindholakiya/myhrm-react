import React, { Component } from 'react';
import { Button, Form, Modal } from "react-bootstrap";
import { faSave, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class AddDepartment extends Component {

    constructor(props) {
        super(props);

        this.state = {
            departmentName: null,
            parentDepartment: null,
            departmentStatus: false,
        }
    }

    HandleModal = () => {
        this.props.hideModal()
    };

    createDepartment() {
        fetch('http://localhost:3000/departments', {
            method: 'Post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: null,
                DeptName: this.state.departmentName,
                Parent: this.state.parentDepartment,
                Status: this.state.departmentStatus,
            })
        }).then((result) => {
            result.json().then((resp) => {
                this.HandleModal();
                alert("Department has been added successfully.");
            })
        })
    }

    render() {
        return (
            <Modal show={this.props.showModal} centered backdrop={"static"} onHide={this.props.hideModal}>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Create Department
                    </Modal.Title>
                </Modal.Header>
                <Form>
                    <Modal.Body>
                        <Form.Group controlId="departmentName">
                            <Form.Label>Department Name</Form.Label>
                            <Form.Control
                                type="text"
                                name="departmentName"
                                required
                                placeholder="Enter department name"
                                onChange={(event) => this.setState({ departmentName: event.target.value })} />
                        </Form.Group>
                        <Form.Group controlId="parentDepartment">
                            <Form.Label>Parent Department</Form.Label>
                            <Form.Control as="select"
                                onChange={(event) => this.setState({ parentDepartment: event.target.value })}>
                                <option value="">Choose parent department</option>
                                <option value="1">Management</option>
                                <option value="2">Account</option>
                                <option value="3">Information Technology</option>
                                <option value="4">Human Resource</option>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group controlId="departmentStatus">
                            <Form.Label>Active</Form.Label>
                            <Form.Check
                                name="departmentStatus"
                                type="switch"
                                id="custom-switch"
                                label=" "
                                value={this.state.departmentStatus}
                                onChange={() => this.setState({ departmentStatus: !this.state.departmentStatus })}
                            />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="success" onClick={() => this.createDepartment()}><FontAwesomeIcon icon={faSave} /> Save</Button>
                        <Button variant="secondary" onClick={this.props.hideModal}><FontAwesomeIcon
                            icon={faTimes} /> Cancel</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        );
    }
}

export default AddDepartment;