import React, { useEffect, useState } from 'react';
import { Button, Table } from "react-bootstrap";
import AddDesignation from "../Designations/AddDesignation";
import { faCertificate, faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import SuccessToast from '../../Utilities/SuccessToast';

function Designations() {

    const [showModal, setShowModal] = useState(false);
    const [designation, setDesignation] = useState([]);
    const [refresh, setRefresh] = useState(0)
    const [showToast, setShowToast] = useState(false);

    const handleShow = () => setShowModal(true);
    const handleClose = () => setShowModal(false);
    const handleShowToast = () => setShowToast(true);
    const handleCloseToast = () => setShowToast(false);


    useEffect(() => {
        async function fetchData() {
            const res = await fetch("http://localhost:3000/designations");
            const result = await res.json();
            setDesignation(result)
        }
        fetchData();
    }, [refresh]);

    const reloadGrid = () => setRefresh(refresh + 1);

    return (
        <div className="container-fluid">
            <div className="content-wrap mt-3">
                <div className="row mb-3">
                    <SuccessToast
                        show={showToast}
                        onHide={handleCloseToast}
                    />
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <h4 className="font-weight-bold">
                            <FontAwesomeIcon icon={faCertificate} /> Designations</h4>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6">

                        <Button variant="dark" className="float-right" onClick={handleShow}>+ Add Designation</Button>
                        <AddDesignation
                            show={showModal}
                            onHide={handleClose}
                            refresh={reloadGrid}
                        />
                    </div>
                </div>
                <Table bordered hover responsive size="sm">
                    <thead className="thead-dark">
                        <tr>
                            <th>Emp ID</th>
                            <th>Designation</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {designation.length > 0 ? (
                            designation && designation.map(item => (
                                <tr key={item.id}>
                                    <td>{item.id}</td>
                                    <td>{item.Name}</td>
                                    <td>{item.Status === true ? 'Active' : 'Inactive'}</td>
                                    <td>{item.Updated_At}</td>
                                    <td>{item.Created_At}</td>
                                    <td>
                                        <Button variant={"outline-success"} className="mr-1 btn-sm" href={'edit'}>
                                            <FontAwesomeIcon icon={faEdit} /></Button>
                                        <Button variant={"outline-danger"} className="btn-sm" >
                                            <FontAwesomeIcon icon={faTrash} />
                                        </Button>
                                    </td>
                                </tr>
                            ))
                        ) : (
                                <tr>
                                    <td colSpan={6} className="text-center">No records available in table.</td>
                                </tr>
                            )
                        }

                    </tbody>
                </Table>
            </div>
        </div>
    );
}

export default Designations;