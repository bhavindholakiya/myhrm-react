import React, { useState } from 'react';
import { Button, Form, Modal } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSave, faTimes } from "@fortawesome/free-solid-svg-icons";
import SuccessToast from "../../Utilities/SuccessToast";

function AddDesignation(props) {

    const initialState = { id: '', designationName: '', designationStatus: false };
    const [designation, setDesignation] = useState(initialState);
    const [show, setShow] = useState(props.showModal);
    const [showToast, setShowToast] = useState(false);

    let nowDate = new Date(),
        todayDate = nowDate.getDate() + "/" +
            ("0" + (nowDate.getMonth() + 1)).slice(-2) + "/" +
            nowDate.getFullYear();

    const [currentDate, setCurrentDate] = useState(todayDate);

    const handleClose = () => setShow(props.onHide);

    const createDesignation = (e) => {
        e.preventDefault();
        fetch('http://localhost:3000/designations', {
            method: 'Post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: designation.id,
                Name: designation.designationName,
                Status: designation.designationStatus,
                Updated_At: currentDate,
                Created_At: currentDate
            })
        }).then((result) => {
            result.json().then((resp) => {
                props.onHide();
                // alert("Designation has been created successfully");                
                props.refresh();
                handleShowToast();
                clearState();
            }
            )
        })
    };

    const handleShowToast = () => props.show(true);

    const handleChange = (event) => {
        event.persist();
        const { name, value } = event.target;
        debugger
        setDesignation({ ...designation, [name]: value })
    };

    const handleStatus = () => {
        debugger
        designation.designationStatus = !designation.designationStatus
    };

    const clearState = () => {
        debugger
        setDesignation({ ...initialState });
    };

    return ( 
        <Modal show={props.show} centered backdrop={"static"} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Create Designation</Modal.Title>
            </Modal.Header>
            <Form onSubmit={createDesignation}>
                <Modal.Body>
                    <Form.Group controlId="designationName">
                        <Form.Label>Designation Name</Form.Label>
                        <Form.Control
                            type="text"
                            name="designationName"
                            required
                            placeholder="Enter designation name"
                            value={designation.designationName}
                            onChange={handleChange}
                        />
                    </Form.Group>
                    <Form.Group controlId="designationStatus">
                        <Form.Label>Active</Form.Label>
                        <Form.Check
                            name="designationStatus"
                            type="switch"
                            id="custom-switch"
                            label=""
                            value={designation.designationStatus}
                            onChange={handleStatus}
                        />
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success" type="submit" >
                        <FontAwesomeIcon icon={faSave} /> Save
                </Button>
                    <Button variant="secondary" onClick={handleClose}>
                        <FontAwesomeIcon icon={faTimes} /> Cancel
                </Button>
                </Modal.Footer>
            </Form>
        </Modal>

    );
}

export default AddDesignation;