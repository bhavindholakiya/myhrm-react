import { Button, Col, Row, Toast } from "react-bootstrap";
import React, { useState } from "react";

function SuccessToast(props) {
    const [show, setShow] = useState(props.showToast);

    return (
        <Row>
            <Col xs={6}>
                <Toast show={props.show} onClose={() => setShow(props.onHide)} delay={3000} autohide>
                    <Toast.Header>
                        <img
                            src="holder.js/20x20?text=%20"
                            className="rounded mr-2"
                            alt=""
                        />
                        <strong className="mr-auto">Bootstrap</strong>
                        <small>11 mins ago</small>
                    </Toast.Header>
                    <Toast.Body>Woohoo, you're reading this text in a Toast!</Toast.Body>
                </Toast>
            </Col>
        </Row>
    );
}

export default SuccessToast;