import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import Header from "./Components/Header";
import Dashboard from "./Components/Dashboard";
import Employee from "./Components/Employees/Employee";
import Departments from "./Components/Settings/Department/Departments";
import Designations from "./Components/Settings/Designations/Designations";
import LeaveTypes from "./Components/Settings/Leave-Type/Leave-Types";
import MyLeaves from "./Components/MyLeaves.";
import Footer from "./Components/Footer";


function App() {
    return (
        <Router>
            <Header/>
            <Switch>
                <Route exact path="/" component={Dashboard}/>
                <Route path="/dashboard" component={Dashboard}/>
                <Route path="/employee" component={Employee}/>
                <Route path="/my-leaves" component={MyLeaves}/>
                <Route path="/settings/departments" component={Departments}/>
                <Route path="/settings/designations" component={Designations}/>
                <Route path="/settings/leave-types" component={LeaveTypes}/>
            </Switch>
            <Footer/>
        </Router>

    )
}

export default App;
